import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by E.Krotov on 24.03.2016. (e.krotov@hotmail.com))
 */
public class TestSearch {

    static WebDriver driver;
    static Wait<WebDriver> wait;
    String searchTextClass;
    String searchButtonClass;
    String searchResultClass;
    String searchResult;

    public void test(String url, String searchTextId, String searchButtonClass, String searchResultClass, String searchResult) {
        this.searchTextClass = searchTextId;
        this.searchButtonClass = searchButtonClass;
        this.searchResultClass = searchResultClass;
        this.searchResult = searchResult;

//        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
//        driver = new ChromeDriver();

        driver = new FirefoxDriver();

//System.setProperty("webdriver.ie.driver", "C:\\IEDriverServer.exe");
//        driver = new InternetExplorerDriver();

        wait = new WebDriverWait(driver, 30);

        driver.get(url);
        boolean result;
        try {
            result = firstPageContainsQAANet(searchTextId, searchButtonClass, searchResultClass, searchResult);
        } catch (Exception e) {
            e.printStackTrace();
            result = false;
        } finally {
            driver.close();
        }

        System.out.println("Search test of " + url + " with search query /" + searchResult + "/ " + (result ? " passed." : "failed."));
        if (!result) {
            System.exit(1);
        }
    }

    private static boolean firstPageContainsQAANet(String searchTextId, String searchButtonClass, final String searchResultClass, String searchResult) {

        //type search query
        driver.findElement(By.className(searchTextId)).sendKeys(searchResult + "\n");

        // click search
//            driver.findElement(By.className("button")).click();

        // Wait for search to complete
        wait.until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver webDriver) {
                System.out.println("Searching ...");
                return webDriver.findElement(By.className(searchResultClass)) != null;
            }
        });

        // Look for QAAutomation.net in the results
        return driver.findElement(By.className(searchResultClass)).getText().contains(searchResult);
    }
}

